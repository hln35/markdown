(function () {
    "use strict";

    String.prototype.format = function () {
        var args = arguments;

        return this.replace(/\{(\d+)\}/g, function (match, sub1) {
            return args[sub1];
        });
    };

    var END_ESCAPE = '@@placeholder@@';
    var END_ESCAPE_LENGTH = END_ESCAPE.length;
    var DOUBLE_LF = '\n\n';

    var KEY_DEFAULT_CODE_BLOCK = 'DEFAULT@CODE@BLOCK';
    var KEY_GIT_HUB_CODE_BLOCK = 'GIT@HUB@CODE@BLOCK';
    var KEY_HTML_TO_EXTRACT = 'HTML@TO@EXTRACT';
    var KEY_INLINE_CODE = 'INLINE@CODE';

    var regexpEndWithDoubleNewLine = /\n{2,}$/;
    var regexpList = /^[ ]{0,3}(\d+\.|[*+-])[ \t]+[\s\S]+?\n{2,}(?=\S+)(?![ \t]*(?:[*+-]|\d+\.)[ \t]+)/mg;
    var regexpListItemSeparator = /^[ ]{0,3}(?:\d+\.|[*+-])/m;
    var regexpOLOpenTag = /\d+\./;
    var regexpListItemPrefixEmpty = /^([ ]{4}|\t)(?=\S)|([ ]{3}|\t)/mg;

    var regexpHeader = /^(#+)([^#\n]+)\n/mg;
    var regexpParagraph = /^(?!([><#]|\d+\.|`{3}|\n+)[\s\S])[\s\S]+?(?=\n{2,})/mg;

    var regexpBlockQuote = /^>[ \t]*[\s\S]+?[^\n](?=\n{2,})/mg;
    var regexpBlockQuoteLinePrefix = /^>[ \t]?/mg;

    var regexpDefaultCodeBlock = /^(?:[ ]{4,}|\t+)[\s\S]+?[^\n](?=\n{2,})/mg;
    var regexpDefaultCodeBlockLinePrefix = /^[ ]{4}|\t/mg;
    var regexpGitHubCodeBlock = /^`{3}(([^`]*?)\n([\s\S]+?))`{3}/mg;

    var regexpHtml2Extract = /^[ \t]*<([^>]+)>[\s\S]*?<\/\1>/mg;
    var regexpExtracted = /`{5}[^`\n]+`{5}/mg;

    var regexpInlineCode = /`([^\n`]+)`/mg;
    var regexpInlineBold = /([*_]{2})([^\n]+?)\1/mg;
    var regexpInlineItalic = /([*_])([^\n]+)\1/mg;
    var regexpInlineStrikeThrough = /~{2}(?!~{2}[^\n]+)([^\n]+?)~{2}/mg;

    var regexpInlineLink = /\[([^\[\]]+)\]\(([\s\S]+?)(?:(["'])([^"']+)\3)*\)(?=\s|$)/mg;
    var regexpInlineAutoLink = /\s(?:https?|s?ftp):\/\/\S+\s*/mg;

    var extractedCollection = (function () {
        var coll = {}, idSeed = 0;

        return {
            _makeId: function () {
                return ++idSeed;
            },
            _makeKey: function (keyPrefix) {
                return '`````{0}#{1}`````'.format(keyPrefix, this._makeId());
            },
            set: function (keyPrefix, data) {
                var key = this._makeKey(keyPrefix);
                coll[key] = data;
                return key;
            },
            get: function (key) {
                if (!key)
                    return coll;

                return coll[key];
            },
            clear: function () {
                coll = {};
            }
        };
    })();

    var createQueue = function () {
        var queue = [];
        queue.deal = function (input) {
            var i = 0, len = this.length, item;
            for (; i < len; ++i) {
                item = this[i];
                input = item(input);
            }

            return input;
        };

        return queue;
    };

    var makeProcessQueue = function (input) {
        var processQueue = createQueue(), forList = input + DOUBLE_LF + END_ESCAPE;

        if (forList.search(regexpList) !== -1) {
            processQueue.push(processList);
        }

        if (input.search(regexpBlockQuote) !== -1) {
            processQueue.push(processBlockQuote);
        }

        if (input.search(regexpParagraph) !== -1) {
            processQueue.push(processParagraph);
        }

        if (input.search(regexpHeader) !== -1) {
            processQueue.push(processHeader);
        }

        return processQueue;
    };

    var processBlockQuote = function (input) {
        input = input.replace(regexpBlockQuote, function (match) {
            var processQueue, isNormalMultiLines, inlineProcessQueue;

            match = match.replace(regexpBlockQuoteLinePrefix, '');

            processQueue = makeProcessQueue(match);
            isNormalMultiLines = processQueue.length === 0;

            if (isNormalMultiLines) {
                inlineProcessQueue = makeInlineProcessQueue(match);
                match = inlineProcessQueue.deal(match);
                return '<blockquote><p>{0}</p></blockquote>'.format(match);
            } else {
                return '<blockquote>\n{0}\n</blockquote>'.format(processQueue.deal(match));
            }
        });

        input = input.replace(/\n{2,}(?![><*+-]|[ ]{4,}|\t+)\w+[\s\S]+?(?=<\/blockquote>)/mg, function (match) {
            var inlineProcessQueue = makeInlineProcessQueue(match);
            match = inlineProcessQueue.deal(match);
            return '\n\n<p>{0}</p>\n'.format(match.trim());
        });

        return input;
    };

    var processCodeBlock = function (input) {
        input = input + DOUBLE_LF;

        return input.replace(regexpDefaultCodeBlock, function (match) {
            var codeBlock = match.replace(regexpDefaultCodeBlockLinePrefix, '');
            codeBlock = '<pre>{0}</pre>'.format(codeBlock);
            return extractedCollection.set(KEY_DEFAULT_CODE_BLOCK, codeBlock);
        });
    };

    var processGitHubCodeBlock = function (input) {
        return input.replace(regexpGitHubCodeBlock, function (match, sub1, sub2, sub3) {
            var codeBlock = '<div class="highlight highlight-{0}">\n<pre>{1}</pre>\n</div>'.format(sub2, sub3);
            return extractedCollection.set(KEY_GIT_HUB_CODE_BLOCK, codeBlock);
        });
    };

    var processParagraph = function (input) {
        return input.replace(regexpParagraph, function (match) {
            var inlineProcessQueue = makeInlineProcessQueue(match);

            if (inlineProcessQueue.length === 0)
                return '<p>{0}</p>'.format(match.trim());

            return '<p>{0}</p>'.format(inlineProcessQueue.deal(match));
        });
    };

    var processListItem = function (input) {
        var items,
            out = '',
            i = 0,
            len,
            processQueue,
            normalMultiLinesNeedPTag = false,
            isNormalMultiLines = false,
            isEndWithDoubleNewLine = false;

        items = input.split(regexpListItemSeparator);
        len = items.length;

        items.forEach(function (item) {
            if (item === '') {
                ++i;
                return null;
            }

            item = item.replace(regexpListItemPrefixEmpty, '');

            processQueue = makeProcessQueue(item);

            isNormalMultiLines = processQueue.length === 0 ||
                i === len - 1 && processQueue.length === 1 && processQueue[0] === processParagraph;

            isEndWithDoubleNewLine = regexpEndWithDoubleNewLine.test(item);

            if (isNormalMultiLines) {
                var inlineProcessQueue = makeInlineProcessQueue(item);
                item = inlineProcessQueue.deal(item).trim();

                if (isEndWithDoubleNewLine && (i < len - 1)) {
                    out += '<li><p>{0}</p></li>'.format(item);
                    normalMultiLinesNeedPTag = true;
                } else if (normalMultiLinesNeedPTag) {
                    out += '<li><p>{0}</p></li>'.format(item);
                    normalMultiLinesNeedPTag = false;
                } else {
                    out += '<li>{0}</li>\n'.format(item);
                }
            } else {
                out += '<li>{0}</li>\n'.format(processQueue.deal(item));
            }

            ++i;
        });

        return out;
    };

    var processList = function (input) {
        input += DOUBLE_LF + END_ESCAPE;

        input = input.replace(regexpList, function (match, sub1) {
            var openTag, closeTag, items;

            if (regexpOLOpenTag.test(sub1)) {
                openTag = '<ol>';
                closeTag = '</ol>';
            } else {
                openTag = '<ul>';
                closeTag = '</ul>';
            }

            items = processListItem(match);

            return '\n' + openTag + '\n' + items + closeTag + '\n';
        });

        return input.substr(0, input.length - END_ESCAPE_LENGTH).trim();
    };

    var processHtml = function (input) {
        return input.replace(regexpHtml2Extract, function (match) {
            return extractedCollection.set(KEY_HTML_TO_EXTRACT, match.trim());
        });
    };

    var processHeader = function (input) {
        return input.replace(regexpHeader, function (match, sub1, sub2) {
            var level = sub1.length;
            level = level > 6 ? 6 : level;

            return '<h{0}>{1}</h{2}>'.format(level, sub2.trim(), level);
        });
    };

    var expandExtracted = function (input) {
        return input.replace(regexpExtracted, function (match) {
            var data = extractedCollection.get(match);

            if (typeof data === 'string') {
                return data;
            }
        });
    };

    var makeInlineProcessQueue = function (input) {
        var processQueue = createQueue();

        if (input.search(regexpInlineCode) !== -1) {
            processQueue.push(processInlineCode);
        }

        if (input.search(regexpInlineBold) !== -1) {
            processQueue.push(processInlineBold);
        }

        if (input.search(regexpInlineItalic) !== -1) {
            processQueue.push(processInlineItalic);
        }

        if (input.search(regexpInlineStrikeThrough) !== -1) {
            processQueue.push(processInlineStrikeThrough);
        }

        if (input.search(regexpInlineLink) !== -1) {
            processQueue.push(processInlineLink);
        }

        if (input.search(regexpInlineAutoLink) !== -1) {
            processQueue.push(processInlineAutoLink);
        }

        return processQueue;
    };

    var processInlineCode = function (input) {
        return input.replace(regexpInlineCode, function (match, sub1) {
            var code = '<code>{0}</code>'.format(sub1);
            return extractedCollection.set(KEY_INLINE_CODE, code);
        });
    };

    var processInlineBold = function (input) {
        return input.replace(regexpInlineBold, function (match, sub1, sub2) {
            return '<strong>{0}</strong>'.format(sub2);
        });
    };

    var processInlineItalic = function (input) {
        return input.replace(regexpInlineItalic, function (match, sub1, sub2) {
            return '<em>{0}</em>'.format(sub2);
        });
    };

    var processInlineStrikeThrough = function (input) {
        return input.replace(regexpInlineStrikeThrough, function (match, sub1) {
            return '<del>{0}</del>'.format(sub1);
        });
    };

    var processInlineAutoLink = function (input) {
        return input.replace(regexpInlineAutoLink, function (match) {
            return '<a href="{0}">{1}</a>'.format(match, match);
        });
    };

    var parseUrl = (function () {
        var a = document.createElement('a');

        return function (url) {
            a.href = url;
            return a.href;
        };
    })();

    var processInlineLink = function (input) {
        return input.replace(regexpInlineLink, function (match, sub1, sub2, sub3, sub4) {
            var label = sub1, title = sub4, link = parseUrl(sub2), tpl;

            if (title) {
                tpl = '<a href="{0}" title="{2}">{1}</a>';
            } else {
                tpl = '<a href="{0}">{1}</a>';
            }

            return tpl.format(link, label, title);
        })
    };

    window['markdown'] = function (input) {
        extractedCollection.clear();

        input = processHtml(input);

        input = processGitHubCodeBlock(input);
        input = processList(input);
        input = processCodeBlock(input);
        input = processBlockQuote(input);
        input = processParagraph(input);
        input = processHeader(input);

        input = expandExtracted(input);

        return input;
    };
})();