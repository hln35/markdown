## 简介

这是一个 markdown 的 javascript 实现，更简单的说，这又是一个轮子。他应该还有很多的 bugs， 并且还需要扩展，比如
没有实现 github markdown 中的 [table syntax](https://help.github.com/articles/github-flavored-markdown#tables)

## 缘起

其实想写这个解析器由来已久了，写这个东西的话需要有点 __正则表达式__ 的基础。其实很早的时候就已经简单接触过正则表达式了，最初在
写前端表单验证的时候，网上搜出的正则表达式一大串的，比如验证电话号码，邮箱之类的。看到它们的时候根本看不懂，好奇心驱使我想深入
研究下正则，于是最初的时候我写了一个 javascript date format 脚本。由于年久失修，最初发布的地方已经找不到了，不过它的使用方式类似:

```javascript

var date = new Date();
date.format('Y-m-d H:i:s');

// 如果熟悉 php 的 date format string 的话，应该会想到结果应该如下
// 2014-09-01 00:34:56
```

后来的话其实就不经常用正则了，你看像我这样的专做 web 的渣渣，除了表单验证很少会用到正则的，而且表单验证的正则就那么几样，如果是
运维的同行就幸运了，大把的实例去学习正则(意淫哒~)。

突然有一天看到了腾讯CDC上的一篇文章 [高性能JavaScript模板引擎原理解析](http://cdc.tencent.com/?p=5723)，大概看了下原理之后，
知道其实它的 template 工作方式其实是巧妙的编译下模板字符串然后生成一个函数，核心就是:

	new Function ([arg1[, arg2[, ...argN]],] functionBody)

详见: [Firefox MDN - Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)

补充一下，其实这个奇妙的思路原创也并不是腾讯，而是 jQuery 之父 John Resig 的这篇文章 [JavaScript Micro-Templating](http://ejohn.org/blog/javascript-micro-templating/)
但是腾讯CDC中却没有提及，是不是有想自我提升big的嫌疑。

于是乎我自认为自己不会比别人笨，或者说我想试试自己和腾讯CDC的人到底有多少差距，假期搞了两三天终于也实现了一个 [JsTpl](https://bitbucket.org/hln35/jstpl)。
这应该是第二次用一个小项目去学习正则了，编程的话果然还是得动手的。

## 纯粹的分割

然后又是突然有一天看到 markdown 的介绍，然后就去搜搜资料，搜到了这个 [Markdown 语法说明 (简体中文版)](http://wowubuntu.com/markdown)，英文原文当然就是 markdown 创造者的 blog 了
是这个 [Traditional Markdown](http://daringfireball.net/projects/markdown/)。说实话，刚开始看看是差不多懂了，但是似乎每个解析器的结果之间会有点出入，搞得我晕晕的。哈，就这样找了
个理由让自己去实现一个解析器，目的倒不是想超过别人的，而是通过写解析器的过程去学习 markdown 的语法规则，当然，最主要是学 __正则表达式__ 啦。

最后如你所见，新的轮子就横空出世了，铛铛~

## 几个链接

* [Traditional Markdown](http://daringfireball.net/projects/markdown/)
* [Github Markdown Basic](https://help.github.com/articles/markdown-basics)
* [Github Flavored Markdown](https://help.github.com/articles/github-flavored-markdown)

* [Firefox MDN - RegExp](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp)
* [MSDN - 正则表达式语法](http://msdn.microsoft.com/zh-cn/library/ae5bf541(v=vs.90).aspx)
* [正向零宽先行预测](http://fxck.it/post/50558232873)
* [正向零宽先行预测](http://blog.csdn.net/binjly/article/details/12152235)

## 试一试吧

一起来试试这个轮子吧

1. 将这个轮子整个放到 web 服务器根目录下，然后在浏览器中打开目录，如果
没有更改过 web 服务器默认的 index 文件名的话，就可以看到界面了，否则要加上
index.html 哟(妈蛋，把我当小白吗?)
2. 需要一个测试文件啊有木有，哈，就是这个文件啦，复制粘贴到 input 项中，然后 __轻轻地__ 点击 __PROCESS__ 按钮
3. 那就将就看看吧，哈